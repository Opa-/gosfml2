// Copyright (C) 2012-2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package gosfml2

// #include <SFML/Window.h>
// int getEventType(sfEvent* ev) { return ev->type; }
// sfSizeEvent* getSizeEvent(sfEvent* ev) { return &ev->size; }
// sfKeyEvent* getKeyEvent(sfEvent* ev) { return &ev->key; }
// sfTextEvent* getTextEvent(sfEvent* ev) { return &ev->text; }
// sfMouseMoveEvent* getMouseMoveEvent(sfEvent* ev) { return &ev->mouseMove; }
// sfMouseButtonEvent* getMouseButtonEvent(sfEvent* ev) { return &ev->mouseButton; }
// sfMouseWheelEvent* getMouseWheelEvent(sfEvent* ev) { return &ev->mouseWheel; }
// sfMouseWheelScrollEvent* getMouseWheelScrollEvent(sfEvent* ev) { return &ev->mouseWheelScroll; }
// sfJoystickMoveEvent* getJoystickMoveEvent(sfEvent* ev) { return &ev->joystickMove; }
// sfJoystickButtonEvent* getJoystickButtonEvent(sfEvent* ev) { return &ev->joystickButton; }
// sfJoystickConnectEvent* getJoystickConnectEvent(sfEvent* ev) { return &ev->joystickConnect; }
import "C"
import "log"

/////////////////////////////////////
///		CONSTS
/////////////////////////////////////

type EventType int

const (
	EventTypeClosed                 EventType = C.sfEvtClosed                 ///< The window requested to be closed (no data)
	EventTypeResized                EventType = C.sfEvtResized                ///< The window was resized (data in event.size)
	EventTypeLostFocus              EventType = C.sfEvtLostFocus              ///< The window lost the focus (no data)
	EventTypeGainedFocus            EventType = C.sfEvtGainedFocus            ///< The window gained the focus (no data)
	EventTypeTextEntered            EventType = C.sfEvtTextEntered            ///< A character was entered (data in event.text)
	EventTypeKeyPressed             EventType = C.sfEvtKeyPressed             ///< A key was pressed (data in event.key)
	EventTypeKeyReleased            EventType = C.sfEvtKeyReleased            ///< A key was released (data in event.key)
	EventTypeMouseWheelMoved        EventType = C.sfEvtMouseWheelMoved        ///< The mouse wheel was scrolled (data in event.mouseWheel) (deprecated)
	EventTypeMouseWheelScrolled     EventType = C.sfEvtMouseWheelScrolled     ///< The mouse wheel was scrolled (data in event.mouseWheelScroll)
	EventTypeMouseButtonPressed     EventType = C.sfEvtMouseButtonPressed     ///< A mouse button was pressed (data in event.mouseButton)
	EventTypeMouseButtonReleased    EventType = C.sfEvtMouseButtonReleased    ///< A mouse button was released (data in event.mouseButton)
	EventTypeMouseMoved             EventType = C.sfEvtMouseMoved             ///< The mouse cursor moved (data in event.mouseMove)
	EventTypeMouseEntered           EventType = C.sfEvtMouseEntered           ///< The mouse cursor entered the area of the window (no data)
	EventTypeMouseLeft              EventType = C.sfEvtMouseLeft              ///< The mouse cursor left the area of the window (no data)
	EventTypeJoystickButtonPressed  EventType = C.sfEvtJoystickButtonPressed  ///< A joystick button was pressed (data in event.joystickButton)
	EventTypeJoystickButtonReleased EventType = C.sfEvtJoystickButtonReleased ///< A joystick button was released (data in event.joystickButton)
	EventTypeJoystickMoved          EventType = C.sfEvtJoystickMoved          ///< The joystick moved along an axis (data in event.joystickMove)
	EventTypeJoystickConnected      EventType = C.sfEvtJoystickConnected      ///< A joystick was connected (data in event.joystickConnect)
	EventTypeJoystickDisconnected   EventType = C.sfEvtJoystickDisconnected   ///< A joystick was disconnected (data in event.joystickConnect)
	EventTypeTouchBegan             EventType = C.sfEvtTouchBegan             ///< A touch event began (data in event.touch)
	EventTypeTouchMoved             EventType = C.sfEvtTouchMoved             ///< A touch moved (data in event.touch)
	EventTypeTouchEnded             EventType = C.sfEvtTouchEnded             ///< A touch event ended (data in event.touch)
	EventTypeSensorChanged          EventType = C.sfEvtSensorChanged          ///< A sensor value changed (data in event.sensor)
	EventTypeCount                  EventType = C.sfEvtCount                  ///< Keep last -- the total number of event types
)

/////////////////////////////////////
///		INTERFACES
/////////////////////////////////////

type Event interface {
	Type() EventType
}

///////////////////////////////////////////////////////////////
//	EmptyEvents

// The window lost the focus (no data)
type EventLostFocus struct{}

func (EventLostFocus) Type() EventType {
	return EventTypeLostFocus
}

// The window gained the focus (no data)
type EventGainedFocus struct{}

func (EventGainedFocus) Type() EventType {
	return EventTypeGainedFocus
}

// The mouse cursor entered the area of the window (no data)
type EventMouseEntered struct{}

func (EventMouseEntered) Type() EventType {
	return EventTypeMouseEntered
}

// The mouse cursor left the area of the window (no data)
type EventMouseLeft struct{}

func (EventMouseLeft) Type() EventType {
	return EventTypeMouseLeft
}

// The window requested to be closed (no data)
type EventClosed struct{}

func (EventClosed) Type() EventType {
	return EventTypeClosed
}

///////////////////////////////////////////////////////////////
//	KeyEvent

type eventKey struct {
	Code    KeyCode //< Code of the key that has been pressed
	Alt     int     //< Is the Alt key pressed?
	Control int     //< Is the Control key pressed?
	Shift   int     //< Is the Shift key pressed?
	System  int     //< Is the System key pressed?
}

type EventKeyPressed eventKey
type EventKeyReleased eventKey

func newKeyEventFromC(ev *C.sfKeyEvent) eventKey {
	return eventKey{Code: KeyCode(ev.code), Alt: int(ev.alt), Control: int(ev.control), Shift: int(ev.shift), System: int(ev.system)}
}

func (EventKeyPressed) Type() EventType {
	return EventTypeKeyPressed
}

func (EventKeyReleased) Type() EventType {
	return EventTypeKeyReleased
}

///////////////////////////////////////////////////////////////
//	SizeEvent

type EventResized struct {
	Width  uint //< New width, in pixels
	Height uint //< New height, in pixels
}

func newSizeEventFromC(ev *C.sfSizeEvent) EventResized {
	return EventResized{Width: uint(ev.width), Height: uint(ev.height)}
}

func (EventResized) Type() EventType {
	return EventTypeResized
}

///////////////////////////////////////////////////////////////
//	TextEvent

type EventTextEntered struct {
	Char rune //< Value of the rune
}

func newTextEventFromC(ev *C.sfTextEvent) EventTextEntered {
	return EventTextEntered{Char: rune(uint32(ev.unicode))}
}

func (EventTextEntered) Type() EventType {
	return EventTypeTextEntered
}

///////////////////////////////////////////////////////////////
//	MouseMoveEvent

type EventMouseMoved struct {
	X int //< X position of the mouse pointer, relative to the left of the owner window
	Y int //< Y position of the mouse pointer, relative to the top of the owner window
}

func newMouseMoveEventFromC(ev *C.sfMouseMoveEvent) EventMouseMoved {
	return EventMouseMoved{X: int(ev.x), Y: int(ev.y)}
}

func (EventMouseMoved) Type() EventType {
	return EventTypeMouseMoved
}

///////////////////////////////////////////////////////////////
//	MouseButtonEvent

type eventMouseButton struct {
	Button MouseButton //< Code of the button that has been pressed
	X      int         //< X position of the mouse pointer, relative to the left of the owner window
	Y      int         //< Y position of the mouse pointer, relative to the top of the owner window
}

type EventMouseButtonPressed eventMouseButton
type EventMouseButtonReleased eventMouseButton

func newMouseButtonEventFromC(ev *C.sfMouseButtonEvent) eventMouseButton {
	return eventMouseButton{Button: MouseButton(ev.button), X: int(ev.x), Y: int(ev.y)}
}

func (EventMouseButtonPressed) Type() EventType {
	return EventTypeMouseButtonPressed
}

func (EventMouseButtonReleased) Type() EventType {
	return EventTypeMouseButtonReleased
}

///////////////////////////////////////////////////////////////
//	MouseWheelEvent (deprecated)

type EventMouseWheelMoved struct {
	Delta int //< Number of ticks the wheel has moved (positive is up, negative is down)
	X     int //< X position of the mouse pointer, relative to the left of the owner window
	Y     int //< Y position of the mouse pointer, relative to the top of the owner window
}

func newMouseWheelEventFromC(ev *C.sfMouseWheelEvent) EventMouseWheelMoved {
	return EventMouseWheelMoved{Delta: int(ev.delta), X: int(ev.x), Y: int(ev.y)}
}

func (EventMouseWheelMoved) Type() EventType {
	return EventTypeMouseWheelMoved
}

///////////////////////////////////////////////////////////////
//	MouseScrollEvent

type EventMouseWheelScrolled struct {
	Wheel MouseWheel
	Delta float32
	X     int
	Y     int
}

func newMouseWheelScrollEventFromC(ev *C.sfMouseWheelScrollEvent) EventMouseWheelScrolled {
	return EventMouseWheelScrolled{Wheel: MouseWheel(ev.wheel), Delta: float32(ev.delta), X: int(ev.x), Y: int(ev.y)}
}

func (EventMouseWheelScrolled) Type() EventType {
	return EventTypeMouseWheelScrolled
}

///////////////////////////////////////////////////////////////
//	JoystickMoveEvent

type EventJoystickMoved struct {
	JoystickId uint         //< Index of the joystick (in range [0 .. JoystickCount - 1])
	Axis       JoystickAxis //< Axis on which the joystick moved
	Position   float32      //< New position on the axis (in range [-100 .. 100])
}

func newJoystickMoveEventFromC(ev *C.sfJoystickMoveEvent) EventJoystickMoved {
	return EventJoystickMoved{JoystickId: uint(ev.joystickId), Axis: JoystickAxis(ev.axis), Position: float32(ev.position)}
}

func (EventJoystickMoved) Type() EventType {
	return EventTypeJoystickMoved
}

///////////////////////////////////////////////////////////////
//	JoystickButtonEvent

type eventJoystickButton struct {
	JoystickId uint //< Index of the joystick (in range [0 .. JoystickCount - 1])
	Button     uint //< Index of the button that has been pressed (in range [0 .. JoystickButtonCount - 1])
}

func newJoystickButtonEventFromC(ev *C.sfJoystickButtonEvent) eventJoystickButton {
	return eventJoystickButton{JoystickId: uint(ev.joystickId), Button: uint(ev.button)}
}

type EventJoystickButtonPressed eventJoystickButton
type EventJoystickButtonReleased eventJoystickButton

func (EventJoystickButtonPressed) Type() EventType {
	return EventTypeJoystickButtonPressed
}

func (EventJoystickButtonReleased) Type() EventType {
	return EventTypeJoystickButtonReleased
}

///////////////////////////////////////////////////////////////
//	JoystickConnectEvent

type eventJoystickConnection struct {
	JoystickId uint //< Index of the joystick (in range [0 .. JoystickCount - 1])
}

type EventJoystickConnected eventJoystickConnection
type EventJoystickDisconnected eventJoystickConnection

func newJoystickConnectEventFromC(ev *C.sfJoystickConnectEvent) eventJoystickConnection {
	return eventJoystickConnection{JoystickId: uint(ev.joystickId)}
}

func (EventJoystickConnected) Type() EventType {
	return EventTypeJoystickConnected
}

func (EventJoystickDisconnected) Type() EventType {
	return EventTypeJoystickDisconnected
}

///////////////////////////////////////////////////////////////
//standard event handling method used by Window & RenderWindow

func handleEvent(cEvent *C.sfEvent) (ev Event) {
	evType := EventType(C.getEventType(cEvent))
	switch evType {
	case EventTypeResized:
		ev = newSizeEventFromC(C.getSizeEvent(cEvent))
	case EventTypeClosed:
		ev = EventClosed{}
	case EventTypeLostFocus:
		ev = EventLostFocus{}
	case EventTypeGainedFocus:
		ev = EventGainedFocus{}
	case EventTypeTextEntered:
		ev = newTextEventFromC(C.getTextEvent(cEvent))
	case EventTypeKeyReleased:
		ev = (EventKeyReleased)(newKeyEventFromC(C.getKeyEvent(cEvent)))
	case EventTypeKeyPressed:
		ev = (EventKeyPressed)(newKeyEventFromC(C.getKeyEvent(cEvent)))
	case EventTypeMouseWheelMoved:
		ev = newMouseWheelEventFromC(C.getMouseWheelEvent(cEvent))
	case EventTypeMouseWheelScrolled:
		ev = newMouseWheelScrollEventFromC(C.getMouseWheelScrollEvent(cEvent))
	case EventTypeMouseButtonReleased:
		ev = (EventMouseButtonReleased)(newMouseButtonEventFromC(C.getMouseButtonEvent(cEvent)))
	case EventTypeMouseButtonPressed:
		ev = (EventMouseButtonPressed)(newMouseButtonEventFromC(C.getMouseButtonEvent(cEvent)))
	case EventTypeMouseMoved:
		ev = newMouseMoveEventFromC(C.getMouseMoveEvent(cEvent))
	case EventTypeMouseLeft:
		ev = EventMouseLeft{}
	case EventTypeMouseEntered:
		ev = EventMouseEntered{}
	case EventTypeJoystickButtonReleased:
		ev = (EventJoystickButtonReleased)(newJoystickButtonEventFromC(C.getJoystickButtonEvent(cEvent)))
	case EventTypeJoystickButtonPressed:
		ev = (EventJoystickButtonPressed)(newJoystickButtonEventFromC(C.getJoystickButtonEvent(cEvent)))
	case EventTypeJoystickMoved:
		ev = newJoystickMoveEventFromC(C.getJoystickMoveEvent(cEvent))
	case EventTypeJoystickDisconnected:
		ev = (EventJoystickDisconnected)(newJoystickConnectEventFromC(C.getJoystickConnectEvent(cEvent)))
	case EventTypeJoystickConnected:
		ev = (EventJoystickConnected)(newJoystickConnectEventFromC(C.getJoystickConnectEvent(cEvent)))
	default:
		log.Fatalf("Unknown event: %+v\n", evType)
	}
	return
}
